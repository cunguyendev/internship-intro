### Welcome to [Code Level Team](https://codelevel.000webhostapp.com)

[![code-level-img]][code-level]

[code-level-img]: http://brinesoftsolutions.com/wp-content/uploads/2016/09/custom-web-development-company-in-chennai-1024x400.jpg "Web development"
[code-level]: https://codelevel.000webhostapp.com

> View [<b>readme</b>](README.md) or [<b>contact me</b>](#contact)

## Setup environment
- Clear proxy address `git config --global http.proxy ""`
- Clear proxy address in npm `npm config delete proxy` or `npm config delete https-proxy`
- Update npm latest version `npm install`

## Some tips
- Animation `animation: App-logo-spin infinite 20s linear;`

## Contact
|[<b>Github</b>][github]|[<b>Gitlab</b>][gitlab]|[<b>Bitbucket</b>][bitbucket]|[<b>Instagram</b>][instagram]|
|         :---:         |         :---:        |            :---:             |            :---:            |
|[![github-img]][github]|[![gitlab-img]][gitlab]|[![bitbucket-img]][bitbucket]|[![instagram-img]][instagram]|


[bitbucket]: https://bitbucket.org/cunguyendev/ "[Cư Nguyễn Dev] - Bitbucket"
[bitbucket-img]: http://rtfm.co.ua/wp-content/uploads/2015/06/bitbucket-logo.png "[Cư Nguyễn Dev] - Bitbucket"

[github]: https://github.com/cunguyendev "[Cư Nguyễn Dev] - Github"
[github-img]: https://desktop.github.com/images/desktop-icon.svg "[Cư Nguyễn Dev] - Github"

[gitlab]: https://gitlab.com/cunguyendev "[Cư Nguyễn Dev] - Gitlab"
[gitlab-img]: https://assets.gitlab-static.net/assets/gitlab_logo-7ae504fe4f68fdebb3c2034e36621930cd36ea87924c11ff65dbcb8ed50dca58.png "[Cư Nguyễn Dev] - Gitlab"

[instagram]:https://instagram.com/inline98/ "[Cư Nguyễn Dev] - Instagram"
[instagram-img]:https://cdn.zapier.com/storage/developer/30129480e943e5b3e334394168ae16a2.128x128.png "[Cư Nguyễn Dev] - Instagram"

- Full name: Nguyễn Văn Cư
- Phone: (+84)1652213396
- Email: 

  >  <a>cunguyen.dev@gmail.com</a>

  >  <a>cunguyen.dev@outlook.com</a>

  >  <a>cunguyen.dev@zoho.com</a>

![](https://github.com/docker/dockercraft/raw/master/docs/img/contribute.png?raw=true)