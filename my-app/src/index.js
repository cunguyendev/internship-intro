import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import registerServiceWorker from './registerServiceWorker';

function tick() {

    const timer = (
        <p>{new Date().toLocaleTimeString()}</p>
    );
    ReactDOM.render(timer, document.getElementById('timer'));
}

// ReactDOM.render(<App />, document.getElementById('root'));
setInterval(tick, 1000);
registerServiceWorker();
